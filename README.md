# IMPACT Specification

### This file is a placeholder until we get Confluence set up!

-------

## What is IMPACT?
- IMPACT, a.k.a. d**I**stributed **M**inecraft **P**lugin **A**rchitecture : **C**ore **T**echnology is an open specification for Minecraft Server plugins designed for use by large networks that need the infinite scalability and fault tolerance of distributed systems.

- IMPACT-S is a SpongeMC-specific implementation of this specification in the form of a common framework and example plugins.
- The IMPACT suite includes
  - The IMPACT common specification
  - The IMPACT code style guide
  - The IMPACT architecture specification
  - The Mercury@IMPACT protocol

- The IMPACT-S suite includes:
	- Envoy - The common backend/hub/manager/messaging platform that brings the IMPACT-S ecosystem together.
	- Bearer - A common framework library for IMPACT-enabled plugins.
	- Steward - A support ticket system designed for infinite scalability and multi-server support.
	- Gatekeeper - A common authentication system with support for website integration, inventory syncing over multiple servers and more.

## The IMPACT Architecture
The IMPACT architecture suite describes a distributed plugin system as a whole, the individual components of the system and the common protocol dubbed Mercury after the Roman god of merchants, travellers and transporters of goods who often figured as a messenger of the gods in Romand mythology.

## Mercury@IMPACT
Mercury is the common protocol shared by all the IMPACT components. Mercury@IMPACT is the common abstract specification of the protocol. Mercury@IMPACT-S is an example implementation as a part of the IMPACT-S suite.

**{PLACEHOLDER}**

## Envoy
Envoy is an example backend implementation for the IMPACT-S suite. It is a standalone application, separate from the plugins, acting as a connector between the plugins, external application and web services.

The Envoy example implementation is in written in Go.

**{PLACEHOLDER}**

## Bearer
Bearer is the common framework library used by all the IMPACT-S plugins. It helps abstract the communication architecture between the IMPACT-S components and Envoy.

**{PLACEHOLDER}**

## Steward
Steward is an example plugin using the IMPACT architecture. The steward plugin is a simple, yet very powerful support ticketing system available both in-game and as a web application. One instance of Steward can be used across multiple servers in one network or even multiple server that are completely independent.

**{PLACEHOLDER}**

## Gatekeeper
Gatekeeper is an example plugin using the IMPACT architecture. The gatekeeper plugin acts as a common authentication backend for Websites, Forums, Web Applications and in-game players. Gatekeeper can be used to synchronize user information across the game, individual IMPACT-S components, web applications etc. Gatekeeper can also sync in-game inventories, experience points, health and hunger bar state and more across multiple independent servers.

**{PLACEHOLDER}**